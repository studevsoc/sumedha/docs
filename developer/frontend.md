## 1 . Setup Workspace : Linux


### install docker

> https://docs.docker.com/engine/install/

  - ` docker --version `

  - `sudo groupadd docker`

  - `sudo usermod -aG docker $USER`

  - `newgrp docker`




### install vscode and vscode extensions

 > https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker

 > https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack

### Run the Workspace

 - select remote dev icon `><`

 ![vscode](../images/developer/vscode1.png)

 - select ` Open Folder in Container`

 ![vscode container](../images/developer/vscode2.png)

 - select the cloned sumdeha frontend Folder

 ![vscode workdir](../images/developer/vscode3.png)

 - wait until finish the docker build finishes
 - start a new bash after completing the build

 ![vscode docker](../images/developer/vscode4.png)

## Setup Workspace : Windows


#### install docker

> https://docs.docker.com/engine/install/

  ` docker --version `


### install vscode and vscode extensions

   > https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker

   > https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack

### Run the Workspace

   - select remote dev icon `><`

   ![vscode](../images/developer/vscode1.png)

   - select ` Open Folder in Container`

   ![vscode container](../images/developer/vscode2.png)

   - select the cloned sumdeha frontend Folder

   ![vscode workdir](../images/developer/vscode3.png)

   - wait until finish the docker build finishes

   - start a new bash after completing the build

   ![vscode docker](../images/developer/vscode4.png)

## 2. Connect your android device to workspace


#### Install/Configure adb if not available in your terminal/power shell

 - check adb installation by entering this command on your power shell

    `adb --version`
 - if output is similar to below then you can directly jumb to Connect via wifi part and skip below installation steps

    ![vscode docker](../images/developer/adb.png)

 - install android-adb using platform tools

 > https://developer.android.com/studio/releases/platform-tools#downloads



#### 3. Connect via wifi

 - enable debugging mode in andriod developer options

 - connect your device to pc

 - connect android device and system in same wifi

 - open a terminal/power shell

 - use below commands

   `adb tcpip 5555`

   `adb connect 192.168.43.9:5555` # this ip <192.168.43.9> your android device ip.

   `adb devices`

   - get android device ip from wifi > info

   - if you are using your android device as an hotspot then, get your ip address by :

    - install termux and open the termux in android
    - `pkg install net-tools`
    - `ifconfig -a | grep 192.168.`
    - you will get the full ip address

- go back to your workspace bash in vscode

  - then  ` adb connect 192.168.43.9:5555 `


## 4. Test the workspace

- `flutter doctor`

  ![vscode docker](../images/developer/docker.png)

- if output similar to above then you need to go back to step 3

  ![vscode docker](../images/developer/docker.png)

- above output leads to sucessfull config of the workspace.

- now just run `flutter run` and relax  :smile:
