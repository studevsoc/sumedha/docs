# Sumedha Sample Design

## These are the sample designs shared

#### [Video (Recorded Screen)](https://docs.sumedha.ml/misc/sample_design.html) <!-- -->

> #### Login Page

![Login](images/samples/Sumedha_img1.jpg)

> #### Home Page

![Home](images/samples/Sumedha_img2.jpg)


> #### Menu - 1

![Menu-1](images/samples/Sumedha_img3.jpg)

> #### Menu - 2

![Menu-2](images/samples/Sumedha_img4.jpg)
