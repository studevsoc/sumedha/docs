# Sumedha Academy

This is a software ecosystem that helps educators provide visual and textual content for students.

- The educators and students will primarily interact with the mobile app.
- The educators can add/update/delete content.
- There will be no user registration feature for the app.
- Logins are set by the admins.
- The app will be able to have organize videos and notes based on various subjects


### Legal
All compenonets will primarily be licensed under GPL V3.
The contents licensing is the creators choice.

